from constants import OUTPUT_LOCATION as OUT
import pandas as pd
import datetime as dt
import sqlite3
from constants import JIRA_EXPORT_LOCATION
from constants import DB_LOCATION as DB


# "Smart" export that builds a temporary table from both CN Dashboard reports, and then only makes JSON for nfeed
# of CNs that appear in the RT Jira export. This way our JSON file is only the size needed, instead of including
# everything and handing the processing off to Jira. This is much faster.
def rt_smart_jira_export():
    conn = sqlite3.connect(DB + "CNs.db")
    c = conn.cursor()

    # Build a temp table unioning all in process and released CNs, setting column names equal to the appropriate
    # JSON names
    query = """
        CREATE TEMP TABLE t AS
        SELECT
        [CN Number] AS CN,
        [CN Status/workflow step] AS Status,
        [Current Approver] AS Owner,
        [Date of Last Status] AS Date
        FROM CN_inProcess
        UNION
        SELECT
        [CN Number] AS CN,
        [Release Status] AS Status,
        [CN Initiator] AS Owner,
        [Released Date] AS Date
        FROM CN_released
    """
    c.execute(query)
    conn.commit()

    # Now build a dataframe from the unioned table to populate the JSON, based on a join of the temp table to the
    # jira table. We want to use the jira table to only get the relevant CNs, and then use the CN number as the index
    # to grab the info we need for nfeed.
    query = """
        SELECT DISTINCT
        j.CN AS CN_Num,
        t.Status,
        t.Owner,
        t.Date
        FROM jira AS j
        INNER JOIN t ON j.CN = t.CN
    """
    df = pd.DataFrame(pd.read_sql_query(query, conn))
    conn.close()

    # since Jira is a little diva, we have to iterate over the dataframe rows and fix the date format
    # this is done by using strptime to get a datetime object, then strftime to export it in the correct string format
    for index, row in df.iterrows():
        if row['Date']:  # sometimes it's blank and the next two operations fail on null input
            date = dt.datetime.strptime(row['Date'], '%Y-%m-%d %H:%M:%S')
            row['Date'] = dt.datetime.strftime(date, '%d/%b/%Y')

    js = '{"CN": ' + df.to_json(orient="records") + '}'  # convert to a string so we can do processing before export
    jsout = open(JIRA_EXPORT_LOCATION + "nfeed_output.json", "w")
    jsout.write(js.replace('null', '\"\"'))  # replace the null values with blanks
    jsout.close()
