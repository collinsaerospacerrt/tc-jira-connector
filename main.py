#################################################################################################
# This script reads excel exports from various locations into a db
# After import, various operations are run to provide useful data combined from several sources
# Also, CN information is exported as JSON for Jira to use
# Edit constants.py to change settings
# If installing in a new location, a template CNs.db will have to be created. No tables are
#   required for the first run as they will be created automatically based on input data.
# Additional information can be found in KNB-199 (https://jira-ws.beav.com/browse/KNB-199)
# ---
# Author: Micah Carter (micah.carter@collinsaerospace.com) - RRT
#
#
# Dependencies:
#   pandas (conda)
#   xlrd (conda)
#   xlsxwriter (conda)
#   schedule (pip)
#   toolz (pip)
#################################################################################################

import schedule
import time
from updatedb import updatedb
import json_export
from constants import UPDATE_DB
from constants import LOG_FILE_LOCATION
import reports as r
import sys
import logging

# Setup logging
logger = logging.getLogger('log')
logger.setLevel(logging.DEBUG)
fh = logging.FileHandler(LOG_FILE_LOCATION + 'run.log')
fh.setLevel(logging.DEBUG)  # file handler has debug level logging
ch = logging.StreamHandler()
ch.setLevel(logging.ERROR)  # only errors need to go to the screen
# create formatter and add it to the handlers
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
ch.setFormatter(formatter)
# add the handlers to the logger
logger.addHandler(fh)
logger.addHandler(ch)
# first log entry
logger.info('Logging initialized.')


# the "job" function is everything we want to run daily
def job():
    # run the DB update unless we've flagged it not to run for debugging
    if UPDATE_DB:
        updatedb()
        logger.info('DB Updated')

    # run all the reports
    try:
        r.rrt_dump()
        logger.info('RRT Dump Report Complete')
    except Exception:
        logger.error('RRT Data dump failed.')
    try:
        r.need_to_released()
        logger.info('Items to be Production Verified Complete')
    except Exception:
        logger.error('Pending Production Release report failed.' + sys.exc_info())
    try:
        r.metrics_ecp()
        logger.info('metrics_ecp Report Complete')
    except Exception:
        logger.error("Metrics ECP report failed." + sys.exc_info())
    try:
        r.metrics_all()
        logger.info('metrics_all Complete')
    except Exception:
        logger.error("Metrics ALL report failed." + sys.exc_info())
    try:
        r.long_dwell_time()
        logger.info('Long Dwell Times Report Complete')
    except Exception:
        logger.error("Long Dwell report failed." + sys.exc_info())
    try:
        r.zombie_rt()
        logger.info('rt_zombies Complete')
    except Exception:
        logger.error("Zombie report failed." + sys.exc_info())
    try:
        r.blocking_cn()
        logger.info('Blocking CN Complete')
    except Exception:
        logger.error("Blocking CN report failed." + sys.exc_info())

    # do the json export for jira
    try:
        json_export.rt_smart_jira_export()
        logger.info('Jira Export Complete')
    except Exception:
        logger.error("Jira JSON export failed" + sys.exc_info())

    logger.info("Reports Done")


schedule.every().day.at("07:45").do(job)
# schedule.every(5).minutes.do(job)

job()  # uncomment to run job() without waiting for the scheduler, for debugging

# loop runs any pending schedule jobs then sleeps for 10 seconds
while True:
    # print('\rRunning ' + datetime.now().strftime('%d-%b-%Y (%H:%M:%S.%f)'))
    schedule.run_pending()
    time.sleep(300)
