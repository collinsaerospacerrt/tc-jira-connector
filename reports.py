import datetime
import sqlite3
from dbops import db_out
from dbops import get_col_widths
import pandas as pd
from numpy_utils import busday_count_mask_NaT

# bring in constants
from constants import OUTPUT_LOCATION as OUT
from constants import DB_LOCATION as DB


# Everything from jira, merged with an relevant CN data from TC
def rrt_dump():
    all_rrt_sql = """
        SELECT *
        FROM jira
        LEFT JOIN CN_inProcess
        ON jira.CN = CN_inProcess.[CN Number]
        LEFT JOIN CN_released
        ON jira.CN = CN_released.[CN Number]
    """
    db_out(all_rrt_sql, "All RRT Issues with in-process CN.xlsx", "Key")


# this creates a spreadsheet of anything that needs to be moved to production verification in jira
def need_to_released():
    released_sql = """
        SELECT DISTINCT J.Key, J.CN, J.[Lead DE POC], J.Assignee, R.[CN Initiator], R.[Released Date] FROM jira J
        INNER JOIN CN_released R
        ON J.CN = R.[CN Number]
        WHERE J.Status = "Team Center (ECP)"
    """
    db_out(released_sql, "Items to be Production Verified.xlsx", "Key")


# Nathan's Metrics ECP query from Jira, but with TC data
def metrics_ecp():
    sql = """
        SELECT J.Key,
        J.Summary,
        P.[Date of Last Status] AS [TC Update Time], 
        P.[CN Status/workflow step] AS [(TC_Status)],
        P.[Current Approver] AS [(TC_Status Owner)],
        J.CN,
        J.Status,
        J.Assignee,
        J.[Due Date],
        J.[OEM Authorization],
        P.[Engineering Recommended Effectivity/Disposition],
        J.[Component],
        J.[Lead DE POC],
        J.Reporter,
        J.Created,
        J.[Design Engineer],
        J.[Additional CN-1],
        J.[FPY],
        J.[Complexity],
        J.[Time in Work]
        FROM jira AS J
        LEFT JOIN CN_inProcess AS P
        ON J.CN = P.[CN Number]
        WHERE J.[Lifecycle Phase] = "Engineering Change Proposal (ECP)"
    """
    db_out(sql, "metrics_ecp.xlsx", "Key")


def metrics_all():
    sql = """
        SELECT J.Key,
        J.Summary,
        P.[Date of Last Status] AS [TC Update Time], 
        P.[CN Status/workflow step] AS [(TC_Status)],
        P.[Current Approver] AS [(TC_Status Owner)],
        J.CN,
        J.Status,
        J.Assignee,
        J.[Date to TC],
        J.[OEM Authorization],
        P.[Engineering Recommended Effectivity/Disposition],
        J.[Component],
        J.[Lead DE POC],
        J.Reporter,
        J.Created,
        J.[Design Engineer],
        J.[Additional CN-1],
        R.[Released Date] AS [Resolved],
        J.[Due Date],
        J.[Date to Work],
        J.[FPY],
        J.[Complexity],
        J.[Time in Work],
        J.[Remaining Estimate],
        J.[Time Spent]
        FROM jira AS J
        LEFT JOIN CN_inProcess AS P
        ON J.CN = P.[CN Number]
        LEFT JOIN CN_released AS R
        ON J.CN = R.[CN Number]
    """
    conn = sqlite3.connect(DB + "CNs.db")
    df = pd.DataFrame(pd.read_sql_query(sql, conn))
    conn.close()

    # Move some dates to datetime objects
    df['Date to TC'] = pd.to_datetime(df['Date to TC'])
    df['Due Date'] = pd.to_datetime(df['Due Date'])
    df['Date to Work'] = pd.to_datetime(df['Date to Work'])

    # Perform date math
    # .values.astype('datetime64[D]') converts pandas datetimes to numpy datetimes because that makes sense
    df['Due_Date_Delta'] = busday_count_mask_NaT(df['Due Date'].values.astype('datetime64[D]'), df['Date to TC'].values.astype('datetime64[D]'))
    df['Days_In_Work'] = busday_count_mask_NaT(df['Date to Work'].values.astype('datetime64[D]'), df['Date to TC'].values.astype('datetime64[D]'))

    df.set_index("Key", inplace=True)

    filename = OUT + 'metrics_all.xlsx'
    writer = pd.ExcelWriter(filename, engine='xlsxwriter')

    df.to_excel(writer, sheet_name='Sheet1')
    worksheet = writer.sheets['Sheet1']
    # set the column widths and save the workbook
    for i, width in enumerate(get_col_widths(df)):
        width = 40 if width > 40 else width  # cap the width at 40 characters - some of these columns are busy
        worksheet.set_column(i, i, width)
    writer.save()


# Report of CNs that haven't changed TC status in over 4 weeks, sorted by oldest first
def long_dwell_time():
    sql = """
        SELECT DISTINCT J.Key,
        J.CN,
        P.[CN Initiator] AS Originator,
        P.[Date of Last Status] AS [TC_Update_Time],
        P.[CN Status/workflow step] AS [TC Status],
        P.[Current Approver],
        J.[Lead DE POC],
        J.[Component]
        FROM jira AS J
        INNER JOIN CN_inProcess AS P
        ON J.CN = P.[CN Number]
        WHERE J.[Lifecycle Phase] = "Engineering Change Proposal (ECP)"
    """
    conn = sqlite3.connect(DB + "CNs.db")
    df = pd.DataFrame(pd.read_sql_query(sql, conn))
    conn.close()

    df['TC_Update_Time'] = pd.to_datetime(df['TC_Update_Time'])
    # Now need a subset of df with only values greater than 4 weeks old
    df2 = df[df['TC_Update_Time'] < datetime.datetime.now() - datetime.timedelta(weeks=2)]
    df2.sort_values(by='TC_Update_Time', inplace=True)
    # setting an index after sorting just serves to get rid of the numerical auto index (extra column)
    df2.set_index('Key', inplace=True)

    filename = OUT + 'Long Dwell Times.xlsx'
    writer = pd.ExcelWriter(filename, engine='xlsxwriter')

    df2.to_excel(writer, sheet_name='Sheet1')
    worksheet = writer.sheets['Sheet1']
    # iterate over the columns and set their widths before saving
    for i, width in enumerate(get_col_widths(df2)):
        worksheet.set_column(i, i, width)
    writer.save()


def zombie_rt():
    conn = sqlite3.connect(DB + "CNs.db")
    conn.cursor()
    conn.execute('DROP TABLE IF EXISTS zombies')
    conn.commit()
    sql = """
        CREATE TABLE zombies AS
        SELECT J.Key,
        J.[Summary],
        J.[Lead DE POC],
        J.CN,
        J.Status AS stat,
        P.[CN Number] AS pcn,
        R.[CN Number] As rcn
        FROM jira AS J
        LEFT JOIN CN_inProcess AS P
        ON P.[CN Number] = J.CN
        LEFT JOIN CN_released AS R
        ON R.[CN Number] = J.CN
    """
    conn.execute(sql)
    conn.commit()
    conn.close()
    sql = """
        SELECT [Key] AS [RT Number],
        [Summary],
        [Lead DE POC],
        [CN] AS [CN Number]
        FROM zombies
        WHERE pcn IS NULL
        AND rcn IS NULL
        AND stat = "Team Center (ECP)"
    """
    db_out(sql, "rt_zombies.xlsx", "RT Number")


def blocking_cn():
    sql = """
        SELECT J.Key,
        J.[Summary],
        J.[Blocking CN],
        P.[CN Workflow Type],
        P.[CN Status/workflow step],
        P.[Current Approver],
        P.[Date of Last Status]
        FROM jira AS J
        LEFT JOIN CN_inProcess AS P
        ON P.[CN Number] = J.[Blocking CN]
        WHERE J.[Blocking CN] IS NOT NULL
    """
    db_out(sql, "blocking_CNs.xlsx", "Key")
