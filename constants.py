# Some constants to use throughout the script USE FORWARD SLASHES

OUTPUT_LOCATION = '//WSSite/datafs2$/WS-ENG/Rapid Response Team/Dashboards/Jira-TC Combined Data/'
CN_DASHBOARD_LOCATION = '//WSSite/datafs2$/WS-ENG/Rapid Response Team/Dashboards/TCRA Auto Generated ' \
                        'Reports/CNDashboardReport-RRT/'
JIRA_EXPORT_LOCATION = '//befiler2/JiraTCPD$/'
LOG_FILE_LOCATION = '//befiler2/JiraTCPD$/'
DB_LOCATION = '//befiler2/JiraTCPD$/'

UPDATE_DB = 1  # 1 to run the update, 0 to use whatever's already there
