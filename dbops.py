###
# This is for keeping the SQL functions separate, to facilitate porting to a different DB
###

import sqlite3
from constants import DB_LOCATION as DB
from constants import OUTPUT_LOCATION as OUT
import pandas as pd


# gets the max width of the values in all columns, returns a list
def get_col_widths(dataframe):
    # First we find the maximum length of the index column
    idx_max = max([len(str(s)) for s in dataframe.index.values] + [len(str(dataframe.index.name))])
    # Then, we concatenate this to the max of the lengths of column name and its values for each column, left to right
    return [idx_max] + [max([len(str(s)) for s in dataframe[col].values] + [len(col)]) for col in dataframe.columns]


# pass in a panda dataframe and add all rows as records in a matching table
def write_table(input_df, table):
    try:
        conn = sqlite3.connect(DB + "CNs.db")
        input_df.to_sql(table, conn, if_exists='replace')
    except Exception:
        return "FAIL"
    else:
        return "SUCCESS"


# this function takes a SQL query and a filename and generates an excel file of the query results
def db_out(query, filename, key=None):
    filename = OUT + filename
    writer = pd.ExcelWriter(filename, engine='xlsxwriter')
    conn = sqlite3.connect(DB + "CNs.db")
    conn.cursor()
    df = pd.DataFrame(pd.read_sql_query(query, conn))
    conn.close()
    if key:
        df.set_index(key, inplace=True)
        df.sort_index(inplace=True)
    df.to_excel(writer, sheet_name='Sheet1')
    worksheet = writer.sheets['Sheet1']
    # iterate over columns and set their width before saving
    for i, width in enumerate(get_col_widths(df)):
        width = 40 if width > 40 else width  # cap the width at 40 characters - some of these columns are busy
        worksheet.set_column(i, i, width)
    writer.save()
