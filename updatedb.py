from constants import CN_DASHBOARD_LOCATION as DASH
from constants import JIRA_EXPORT_LOCATION as JIRA
import pandas as pd
from dbops import write_table
import datetime
import os


def updatedb():
    ###
    # create a dataframe from the in-process CN report
    ###

    # Grab the fetch date so we can make sure we have new data, and convert it to a datetime object
    file = DASH + 'CN Dashboard Report-InProcess.xlsx'
    TC_inProcess_data_updated_datetime: datetime.datetime = datetime.datetime.fromtimestamp(os.path.getmtime(file))

    # read the useful part of the report (ignoring first 9 rows) into a dataframe
    inProcess_xl = pd.read_excel(file, 0, 9)
    inProcess_xl.set_index('CN Number', inplace=True)

    ###
    # create a dataframe from the released CN report
    ###

    # Grab the fetch date so we can make sure we have new data, and convert it to a datetime object
    file = DASH + 'CN Dashboard Report-Released.xlsx'
    TC_released_data_updated_datetime: datetime.datetime = datetime.datetime.fromtimestamp(os.path.getmtime(file))

    # read the useful part of the report (ignoring first 6 rows) into a dataframe
    released_xl = pd.read_excel(file, 0, 7)
    released_xl.drop_duplicates("CN Number", inplace=True)
    released_xl.set_index('CN Number', inplace=True)

    ###
    # create a dataframe from the jira export
    ###

    # Grab the fetch date so we can make sure we have new data, and convert it to a datetime object
    file = JIRA + 'jira-export.xlsx'
    jira_data_updated_datetime = datetime.datetime.fromtimestamp(os.path.getmtime(file))

    # read the useful part of the report (ignoring first 6 rows) into a dataframe
    jira_xl = pd.read_excel(file, 0, 0)
    jira_xl.set_index('Key', inplace=True)

    write_table(inProcess_xl, 'CN_inProcess')
    write_table(released_xl, 'CN_released')
    write_table(jira_xl, 'jira')

    log_message = "\ninProcess report timestamp: " + datetime.datetime.strftime(TC_inProcess_data_updated_datetime, '%m/%d/%Y %H:%M:%S')
    log_message += "\nreleased report timestamp: " + datetime.datetime.strftime(TC_released_data_updated_datetime, '%m/%d/%Y %H:%M:%S')
    log_message += "\njira dump timestamp: " + datetime.datetime.strftime(jira_data_updated_datetime, '%m/%d/%Y %H:%M:%S')
